﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SharpDX.DirectInput;
using System.IO.Ports;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<DeviceInstance> inputDevices = new List<DeviceInstance>();
        public SharpDX.DirectInput.DeviceInstance selectedDevice;
        public SerialPort serial = null;
        public SharpDX.DirectInput.DirectInput di;
        private FutureBOX fbox;

        private void Form1_Load(object sender, EventArgs e)
        {
            fbox = new FutureBOX(serial);

            // get input devices
            di = new SharpDX.DirectInput.DirectInput();
            foreach (var deviceInstance in di.GetDevices(SharpDX.DirectInput.DeviceClass.All, SharpDX.DirectInput.DeviceEnumerationFlags.AllDevices))
            {
                inputDevices.Add(deviceInstance);
                cbInputDevices.Items.Add(deviceInstance.ProductName + " - " + deviceInstance.ProductGuid);
            }
            //cbInputDevices.DisplayMember = "ProductName";
            //cbInputDevices.DataSource = inputDevices;
            
            // get COM ports
            string[] ports = SerialPort.GetPortNames();
            cbCOM.DataSource = ports;

        }

        private void cbInputDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedDevice = inputDevices[cbInputDevices.SelectedIndex];
        }

        // start the main loop

        byte[] packet = new byte[10];

        Timer timer1 = null;
        Timer timer2 = null;
        int stilldown = 0;
        
        // start listening to selected input device
        private void button1_Click(object sender, EventArgs evarg)
        {
            if (timer1 != null)
            {
                button1.Text = "START";
                timer1.Stop();
                timer1.Dispose();
                timer1 = null;
                return;
            }

            button1.Text = "STOP";

            var joystick = new Joystick(di, selectedDevice.InstanceGuid);
            var mouse = new Mouse(di);
            joystick.Properties.BufferSize = 32;
            // Acquire the joystick
            joystick.Acquire();
            JoystickUpdate[] u = joystick.GetBufferedData();

            //joystick.RunControlPanel();

            timer1 = new Timer();
            timer1.Interval = 100;
            timer1.Tick += (s, e) =>
            {
                joystick.Poll();

                JoystickUpdate[] uu = joystick.GetBufferedData();
                JoystickState lastState = joystick.GetCurrentState(); //only show the last state

                // switch type
                switch (joystick.Information.Type)
                {
                    case DeviceType.Keyboard:
                        
                        // power
                        if (lastState.Buttons[(int)SharpDX.DirectInput.Key.W-1])
                        {
                            if (packet[0] < 255) packet[1] += 1;
                            if (stilldown++ > 0) packet[1] = (packet[1] + stilldown/10 > 255) ? (byte)255 : (byte)(packet[1] + (byte)(stilldown/15));
                            
                        }
                        else if (lastState.Buttons[(int)SharpDX.DirectInput.Key.S - 1])
                        {
                            if (packet[1] > 0) packet[1] -= 1;
                            if (stilldown++ > 0) packet[1] = (packet[1] - stilldown / 10 < 0) ? (byte)0 : (byte)(packet[1] - (byte)(stilldown / 15));

                        } else
                        {
                            stilldown = 0;
                        }

                        // rotate left/right
                        if (lastState.Buttons[(int)SharpDX.DirectInput.Key.A - 1]) packet[3] = 0x7F;
                        else if (lastState.Buttons[(int)SharpDX.DirectInput.Key.D - 1]) packet[3] = 0xFF;
                        else packet[3] = 0;

                        // forward/backwards
                         if (lastState.Buttons[104]) packet[2] = 0x7F;
                         else if (lastState.Buttons[109]) packet[2] = 0xFF;
                         else packet[2] = 0;

                         // left/right
                         if (lastState.Buttons[106]) packet[4] = 0x7F;
                         else if (lastState.Buttons[107]) packet[4] = 0xFF;
                         else packet[4] = 0;

                        // flip
                        if (lastState.Buttons[(int)SharpDX.DirectInput.Key.Space]) packet[7] = 0x40;
                        else packet[7] = 0;

                        break;
                    case DeviceType.Mouse:
                        break;
                    case DeviceType.Joystick:
                        packet[1] = (byte)(255-lastState.Sliders[0] / 256);

                        int R = lastState.RotationZ / 256 - 127;
                        R = R <= 0 ? Math.Abs(R) : R + 127;
                        packet[3] = (byte)R;

                        int Y = (lastState.Y) / 256 - 127;
                        Y =  Y <= 0 ? Math.Abs(Y) : Y + 127;
                        packet[2] = (byte)Y;

                        int X = (lastState.X) / 256 - 127;
                        X = X <= 0 ? Math.Abs(X) : X + 127;
                        packet[4] = (byte)X;

                        // packet description:
                        // 0x35 0x71 - header
                        // command (0x01)
                        // 0x00 - packet length -- not used at the moment
                        // 4 bytes for xyzr
                        // 2 bytes for additonal trims/pots
                        // 3 bytes for flags (24 flags)
                        // CRC byte (of payload)

                        int flag = 0;
                        for (int i = 0; i < 16; i++)
                        {
                            flag |= Convert.ToByte(lastState.Buttons[i]) << i;
                        }

                        packet[7] = (byte)(flag >> 16);
                        packet[8] = (byte)((flag >> 8) & 0xff);
                        packet[9] = (byte)(flag & 0xff);

                        break;
                }
                // data packet
                packet[0] = 0x01;
               
                // correct speed
                packet[2] = (byte)(packet[2]/numericUpDown1.Value);
                if (packet[2] > 0x80 / numericUpDown1.Value) packet[2] += (byte)(0x80 - 0x80 / numericUpDown1.Value);
                packet[4] = (byte)(packet[4] / numericUpDown1.Value);
                if (packet[4] > 0x80 / numericUpDown1.Value) packet[4] += (byte)(0x80 - 0x80 / numericUpDown1.Value);

                

                if (serial != null)
                {
                    while (serial.BytesToWrite > 0) { }
                    fbox.sendPacket(packet, serial);

                    if (textBox5.TextLength > 500)
                    {
                        textBox5.Text = textBox5.Text.Substring(textBox5.TextLength - 500);
                    }
                    textBox5.AppendText(BitConverter.ToString(packet) + Environment.NewLine);
                }
                
                textBox1.Text = lastState.X.ToString();
                textBox2.Text = lastState.Y.ToString();
                textBox3.Text = lastState.Z.ToString();

                radioButton1.Checked = lastState.Buttons[0];
                radioButton2.Checked = lastState.Buttons[1];
                radioButton3.Checked = lastState.Buttons[2];
                radioButton4.Checked = lastState.Buttons[3];
                radioButton5.Checked = lastState.Buttons[4];
                radioButton6.Checked = lastState.Buttons[5];
                radioButton7.Checked = lastState.Buttons[6];
                radioButton8.Checked = lastState.Buttons[7];

                
            };

            //start the timer
            timer1.Enabled = true;
        }

        // open com port
        private void button3_Click(object sender, EventArgs e)
        {
            if (serial != null)
            {
                timer2.Stop();
                timer2.Dispose();
                timer2 = null;
                
                serial.Close();
                button3.Text = "CONNECT";
                serial = null;
                return;
            }

            timer2 = new Timer();
            timer2.Interval = 500;
            timer2.Tick += (s, ee) =>
            {
                textBox6.AppendText( serial.ReadExisting() );
                if (textBox6.TextLength > 5000)
                {
                    textBox6.Text = textBox6.Text.Substring(textBox6.TextLength - 5000);
                }
            };
            

            button3.Text = "DISCONNECT";

            serial = new SerialPort(cbCOM.SelectedValue.ToString(), 1000000, Parity.None, 8, StopBits.One);
            serial.NewLine = "\r\n";
            serial.ReadTimeout = 1000;
            serial.ReadBufferSize = 10000;
            serial.DtrEnable = true;
            serial.RtsEnable = true;
            serial.WriteBufferSize = 20;
            serial.Open();
            try {
                if (serial.ReadLine() != "FutureBOX") MessageBox.Show("Wrong serial port");
                else textBox5.Text += "FutureBOX" + Environment.NewLine;
            } catch (Exception) { }

            

            try {
                System.Threading.Thread.Sleep(2000);
                serial.ReadExisting();
                byte[] pkt = { 0x35, 0x7A, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A };
                serial.Write(pkt, 0, pkt.Length);
                //do {
                    serial.Read(pkt, 0, 2);
                //} while (pkt[0] != 0x35 || pkt[1] != 0x7a);
                int len = serial.ReadByte();
                cbProtocols.Items.Clear();
                for (int i = 0; i < len; i++)
                {
                    string s = serial.ReadLine();
                    cbProtocols.Items.Add(s);
                }
            } catch (Exception)
            {
                //button3_Click(null, null);
                //return;
            }
            timer2.Start();
        }

        // bind
        private void button2_Click(object sender, EventArgs e)
        {
            // select protocol
            byte[] buffer = { 0x0b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            int proto = cbProtocols.SelectedIndex;
            buffer[1] = (byte)proto;
            fbox.sendPacket(buffer, serial);

            System.Threading.Thread.Sleep(500);

            // bind
            buffer[0] = 0;
            buffer[1] = 0;
            fbox.sendPacket(buffer, serial);
        }


        private void button5_Click(object sender, EventArgs e)
        {
            textBox5.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
        }
    }
}
