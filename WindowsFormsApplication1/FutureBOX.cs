﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class FutureBOX
    {
        SerialPort serial;

        public FutureBOX(SerialPort sport)
        {
            this.serial = sport;
        }

        public bool sendPacket(byte[] data, SerialPort s)
        {
            byte[] packet = new byte[data.Length + 3];

            // header
            packet[0] = 0x35;
            packet[1] = 0x7a;

            int sum = 0;
            for (int i = 0; i< data.Length; i++)
            {
                sum += data[i];
                packet[i + 2] = data[i];
            }

            // CRC
            packet[packet.Length - 1] = (byte)(sum & 0xff);

            s.Write(packet, 0, packet.Length);

            return true;
        }
    }
}
